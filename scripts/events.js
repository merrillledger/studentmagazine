var xhr = new XMLHttpRequest(); 

xhr.onload = function() { /*Loads the JSON FILE*/ 
       if(xhr.status === 200) {
              responseObject = JSON.parse(xhr.responseText);

              var newContent = ''; /***loops through the JSON and creates HTML content */
              for (var i = 0; i < responseObject.events.length; i++) {
                     newContent += '<article class="events">';
                     newContent += '<h4>' + responseObject.events[i].title + '</h4>';
                     newContent += '<p class="eventText">' + responseObject.events[i].description + '</p>';
                     newContent += '<button class="book">BOOK</button>';
                     newContent += '</article>';
              }

              document.getElementById('eventContent').innerHTML = newContent; /**Links the new content to the HTML element */
       }
};

xhr.open('GET', 'scripts/events.json', true); /**Retrieves the JSON File */
xhr.send(null);

